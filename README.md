tutorial: https://medium.com/@phillipmarkpadilla/laravel-5-6-in-docker-with-php-7-2-nginx-1-10-and-mysql-5-7-cdb6c054379c
tutorial: https://github.com/cretueusebiu/laravel-vue-spa/blob/master/resources/assets/js/store/modules/auth.js 
# SETUP 
# Install PHP dependencies with composer inside docker
> docker run --rm -v $(pwd)/site:/app composer install

# Init Voyager
> docker-compose exec app php artisan voyager:install


# Manual Setup
# fix Mysql error
file: `AppServiceProvide.php`
Include class in header:
`use Illuminate\Support\Facades\Schema;`

update function `boot`:
```
public function boot()
{
    Schema::defaultStringLength(191);
}
```

# Add Vojager admin panel
Go in terminal to site/vendor/tcg/voyager
> git pull

# Init Laravel
> docker run --rm -v $(pwd)/site:/app composer install

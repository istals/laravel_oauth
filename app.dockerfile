FROM php:7.2.2-fpm

RUN apt-get update && apt-get install -y mysql-client libpng-dev openssh-server \
    libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev libfreetype6-dev
    
RUN /lib/systemd/systemd-sysv-install enable ssh 

RUN docker-php-ext-configure gd --with-gd --with-webp-dir --with-jpeg-dir \
    --with-png-dir --with-zlib-dir --with-xpm-dir --with-freetype-dir \
    --enable-gd-native-ttf

RUN docker-php-ext-install gd
RUN docker-php-ext-install pdo pdo_mysql